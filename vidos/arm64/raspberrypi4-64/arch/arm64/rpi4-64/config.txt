dtparam=audio=on

display_auto_detect=1

dtoverlay=vc4-kms-v3d
max_framebuffers=2

arm_64bit=1

[all]

[pi4]
# Run as fast as firmware / board allows
arm_boost=1

[all]
enable_uart=1
gpu_mem=256
start_file=start4x.elf
fixup_file=fixup4x.dat
initramfs rootfs.cpio.lz4
kernel=Image
disable_splash=1
