
#!/usr/bin/env bash
set -e
echo "binaries dir" $BINARIES_DIR
BOARD_DIR=$(dirname "$0")

VIDOS_FAT32=vidos_fat32

VIDOS_BUILD=$2_build

VIDOS_RELEASE_PATH=$BINARIES_DIR/vidos_release/
VIDOS_BUILD_PATH=$BINARIES_DIR/vidos_release/$VIDOS_BUILD/
VIDOS_FAT32_PATH=$BINARIES_DIR/vidos_release/$VIDOS_BUILD/$VIDOS_FAT32

mkdir -p $VIDOS_BUILD_PATH/arch/arm64/rpi4-64/$2_kernel
mkdir -p $VIDOS_BUILD_PATH/vidos_ext4/video
mkdir -p $VIDOS_BUILD_PATH/vidos_fat32

echo "created $VIDOS_FAT32 and vidos_ext4 directory"

cp -r $BOARD_DIR/arch/ $VIDOS_BUILD_PATH
cp -r $BOARD_DIR/../../initramfs_overlay $VIDOS_BUILD_PATH

rm $VIDOS_BUILD_PATH/initramfs_overlay/opt/.gitkeep
rm $VIDOS_BUILD_PATH/initramfs_overlay/etc/init.d/.gitkeep
rm $VIDOS_BUILD_PATH/initramfs_overlay/lib/firmware/.gitkeep

#cp $BOARD_DIR/S03Video* $VIDOS_BUILD_PATH
cp $BOARD_DIR/../../vobu.sh $VIDOS_RELEASE_PATH

cp $BINARIES_DIR/Image $VIDOS_BUILD_PATH/arch/arm64/rpi4-64/$2_kernel

#cp $BINARIES_DIR/bzImage $VIDOS_BUILD_PATH/$2_kernel
echo "copied files into $VIDOS_FAT32 directory"

yes | $VIDOS_RELEASE_PATH/vobu.sh -a arm64/rpi4-64 -d $VIDOS_RELEASE_PATH/$VIDOS_BUILD/ -v $BOARD_DIR/../../test_vids/$2_video.*
