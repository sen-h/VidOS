#!/bin/bash
VOBU_VER=v1.4.0-5867cdf
VIDOS_COMP_VER=v2.1.0-5867cdf
DEF_FIRMWARE="none"
DEFAULT_ARCH="x86_64"
RM_EXT_LIB_SUCCESS="Removed external libs"
RM_EXT_LIB_FAIL="External libs are already not installed"
DIR_SET=1
VID_SUPPORTED=1
ARG_VALID=0
ARCH_VALID=1
STYLE_VALID=1
FIRMWARE_VALID=1
BOOTLOADER_VALID=1
PLAYBACK_COUNT_VALID=1
FORMAT_SPECIFIED=1
START_DIR=$PWD
GZIP="gzip"
command -v pigz | grep -q .
if [ $? -eq 0 ]; then GZIP="pigz"; fi

remove_ext_libs() {
        if [ -e ~/.vidos_downloads/arch/$ARCH/.licenceAgreed ]; then
                rm -r ~/.vidos_downloads/arch/$ARCH/*_external_lib/ ~/.vidos_downloads/arch/$ARCH/.licenceAgreed
                echo $RM_EXT_LIB_SUCCESS
        else
                echo $RM_EXT_LIB_FAIL
        fi
exit 0
}

DEFAULT_HELP_TEXT=(
"\n""VidOS build utility $VOBU_VER for default arch: x86_64"
"\n""usage: vobu -d [directory] -v [video filename/dirname/playlist] -s [build style] -g [graphics drivers] -f [format] -r [remove codecs] -b [bootloader/manager]"
"-m [mpv options] -l [playback options] -o [output iso name]"
"\noptions:"
"\n-h help -- print this help text"
"\n""-d directory -- path to vidos components dir, Default paths: /tmp, /opt, ./"
"\n""-v video filename, directory or playlist -- path to video file, directory of video files or playlist of video files, supported video codecs: [ av1 vp8 vp9 h264 ]"
"\n""-s build style -- style of output build, one of: [ disk ram hybrid debug ] Default: ram"
"\n""-g graphics drivers -- binary blob graphics drivers, one or multiple of: [ amdgpu radeon i915 none all (or specific firmware/*.bin) ] Default: none"
"\n""-f format  -- specific video format to use, if omitted one will be autodetected. one of: [ av1 webm avc debug ]"
"\n""-r remove external codecs -- removes/disables OpenH264 and fdk-aac codecs, OpenH264 Video Codec provided by Cisco Systems, Inc."
"\n""-b bootloader/manager for firmware -- select bootloader depending on machine firmware. one of: [ efi bios both ] Default: bios"
"\n""-m mpv options -- extra options to pass to mpv, see: https://mpv.io/manual/stable/#options"
"\n""-l loop count -- how many times to play the video or video files: [ -1 to inf ] Default: 1"
"\n""-o output iso name -- specify a name for the output iso, Defaults to: 'vidos_\$VIDEO_\$FORMAT_\$GRAPHICS_DRIVERS_\$BUILD_STYLE_\$BOOTLOADER_$(date +%F).iso'")

print_help(){
	echo -e ${HELP_TEXT[@]}
	exit $1
}

checkPaths() {
	if [ "$2" == "$3"  ]; then
	        echo "error:" -${1:0:1} "("$1") is unspecified, Please specify a valid" $1 "with -"${1:0:1}
	        ARG_VALID=1
	elif [[ "$1" = -*  ]]; then echo "error:" -${2:0:1} "("$2") is using [" $1 "] as an argument, argument should be a path to a" $2
	        ARG_VALID=1
	elif [ ! -e "$1" ] ; then
	        echo "error: can't find" -${2:0:1} "("$2") \"$1\", Please specify a valid $2 with -${2:0:1}"
		ARG_VALID=1
	fi
}

checkArg() {
	if [ $3 ]; then echo $2":" $3; fi
	if [ $1 -ne 0 ]; then
		echo -e $3 "is not a supported $4 for $2!\
		\nsupported $4"s" for $2 are: [ $5 ]"
		print_help 2
	fi
}

checkOpts() {
	if [[ "$1" = -*  ]]; then echo "error:" -${2:0:1} "("$2") is using [" $1 "] as an argument, argument should be one of: [" $3 "]"; ARG_VALID=1; fi
	if [ "$3" == "$4" ]; then echo "error:" -${1:0:1} "("$1") is unspecified, please specify one of: [" $2 "]"; ARG_VALID=1; fi
}

check_dir() {
	if [ -z $DIR ]; then DIR=$(find /tmp ! -readable -prune -o -name vidos_components-$VIDOS_COMP_VER-*  -print); fi
	if [ -z $DIR ]; then DIR=$(find /opt/vidos/ -type d -name vidos_components-$VIDOS_COMP_VER); fi
	if [ -z $DIR ]; then DIR=$(find -maxdepth 1 -type d -name vidos_components-$VIDOS_COMP_VER); fi
}

list_arch(){
	echo -n $(find $DIR/arch -maxdepth 1 -type d | cut -d "/" -f5 | xargs);
}
early_print_help() {
	check_dir
	if [ -z $DIR ]; then
		checkPaths $DIR "directory"
		echo -e ${DEFAULT_HELP_TEXT[@]}
		echo -e "-a archetecture -- select the output architecture, default: x86_64"
		exit 1
	fi
	if [ -d $DIR/arch/$ARCH ]; then
		if [ -z $ARCH ]; then ARCH=$DEFAULT_ARCH; fi

		find /tmp -maxdepth 1  -name 'vidos_component*' | grep -q vidos
		if [ $? -eq 1 ]; then
			cp -r $DIR /tmp/vidos_components-$VIDOS_COMP_VER-$$
			DIR=/tmp/vidos_components-$VIDOS_COMP_VER-$$
		fi

		. $DIR/arch/$ARCH/config
		echo -e ${HELP_TEXT[@]}
		echo -n "-a archetecture -- select the output architecture, one of: [ "; list_arch; echo " ] Set as:" $ARCH
		exit $1
	else
		echo -e ${DEFAULT_HELP_TEXT[@]}
		echo -n "-a archetecture -- select the output architecture, one of: [ "; list_arch; echo " ] Defaulting to:" $DEFAULT_ARCH
		exit 1
	fi
}

pickCodec(){
	if [ $FORMAT_SPECIFIED -eq 0 ]; then
		for item in "${SUPPORTED_VID_FORMATS[@]}"
		do
			if [ $item == $SELECTED_FORMAT ]; then
				FORMAT_VALID=0
				VID_SUPPORTED=0
				VID_FORMAT=$SELECTED_FORMAT
			fi
		done
	else
		DETECTED_VID_CODEC=$(ffprobe -v quiet -show_streams "$1" | grep -w codec_name | sed -n 1p | cut -d'=' -f2)
		VID_FORMAT=${FORMAT_ARRAY[$DETECTED_VID_CODEC]}
		if  [ $VID_FORMAT ]; then VID_SUPPORTED=0; fi
		checkArg $VID_SUPPORTED "video" $DETECTED_VID_CODEC "codec" "${SUPPORTED_VID_CODECS[*]}"
	fi
	if [ -z $VID_FORMAT ]; then
		checkArg $VID_SUPPORTED "video" $SELECTED_FORMAT "format" "${SUPPORTED_VID_FORMATS[*]}"
	fi

	KERNEL_PATH=$(find $DIR/arch/$ARCH -type d -name $VID_FORMAT"_kernel")
	if [ ! $KERNEL_PATH ]; then echo -e  "vidos" $VID_FORMAT"_kernel not found."; exit 1; fi;

	if [ $VID_FORMAT == $EXT_LIBS_NEEDED ] && [ ! -e ~/.vidos_downloads/arch/$ARCH/.licenceAgreed ]; then
		echo -e ${CODEC_PATTER[@]}
		read -p "Do you agree to the preceeding terms, Yes or no (y/n)? " -N 1 VAL
		sleep 1
		askPermission
	elif [ $VID_FORMAT == $EXT_LIBS_NEEDED ] && [ -e ~/.vidos_downloads/arch/$ARCH/.licenceAgreed ]; then
		VAL=y && askPermission
	fi
}

askPermission() {
	case $VAL in
		y)
			echo "ext libs" $EXT_LIBS_NEEDED
			echo -e ${EXT_LIB_UNINSTALL_INST[@]}
			mkdir -p ~/.vidos_downloads/arch/$ARCH/
			touch ~/.vidos_downloads/arch/$ARCH/.licenceAgreed
			if [ ! -e ~/.vidos_downloads/arch/$ARCH/"$EXT_LIBS_NEEDED"_external_lib ]; then
				installExtLibs
			fi
			cp -r ~/.vidos_downloads/arch/$ARCH/"$EXT_LIBS_NEEDED"_external_lib/usr $DIR/initramfs_overlay
		;;
		n)
			echo -e "\nThat's alright, please select a different video and build style that does not require external binary libraries."
			exit 0
		;;
		*)
			echo -e "\r"
			read -p "Do you agree to the preceeding terms, Yes or no (y/n)? " -N 1 VAL
			askPermission
		;;
	esac
}

while getopts "d:a:rhv:s:g:f:b:m:l:o:" opt; do
	case "$opt" in
		d)
			DIR="$OPTARG"
			echo "Deleting old comp dir in temp (if it exists)"
			if [ ! -d $DIR/$VIDOS_DISKFS ]; then echo "not a valid vidos_components dir!"; exit 1; fi
			find /tmp ! -readable -prune -o -name vidos_components-$VIDOS_COMP_VER-* -print  2> /dev/null -exec rm -r "{}" \;
		;;
		a)
			ARCH="$OPTARG";
		;;
		h)
			early_print_help 0
		;;
		r)
			remove_ext_libs
		;;
		v)
			VIDEO="$OPTARG"
				if [ -d "$VIDEO" ]; then VIDEO_DIR="$VIDEO"; VIDEO_NAME="dir"
				elif [ -f "$VIDEO" ]; then
					if [ $(file --mime-encoding "$VIDEO" | cut -f2 -d " ") = "us-ascii" ]; then
						PLAYLIST="$VIDEO"
						echo $PLAYLIST
						exit 0
					else
						VIDEO_SELECTION+=("$VIDEO");
					fi
				fi
		;;
		s)
			STYLE="$OPTARG"
		;;
		g)
			FIRMWARE="$OPTARG";
			FIRMWARE_SELECTION+=($FIRMWARE)
		;;
		f)
			SELECTED_FORMAT="$OPTARG"
			FORMAT_SPECIFIED=0
		;;
		b)
			BOOTLOADER="$OPTARG";
		;;
		m)
			MPV_OPTS="$OPTARG";
		;;
		l)
			PLAYBACK_COUNT="$OPTARG";
		;;
		o)
			IMG_NAME="$OPTARG";
		;;
	esac
done

if [ $# -eq 0 ]; then
	early_print_help 0
fi

check_dir

if [ -d $DIR/$VIDOS_DISKFS ]; then
	if [ -z $ARCH ]; then ARCH=$DEFAULT_ARCH; fi
	if [ -d $DIR/arch/$ARCH ]; then ARCH_VALID=0; fi
	checkOpts $ARCH "arch" "$(find $DIR/arch -maxdepth 1 -type d -exec bash -c "echo {} | cut -d "/" -f3 " \; )"
	checkArg $ARCH_VALID "architecture" $ARCH "option" "$(find $DIR/arch -maxdepth 1 -type d | cut -d "/" -f5 | xargs)"
	if [ $ARG_VALID -eq 1 ]; then print_help 2; fi
	. $DIR/arch/$ARCH/config
fi

find /tmp -maxdepth 1  -name 'vidos_component*' | grep -q vidos
if [ $? -eq 1 ]; then
	cp -r $DIR /tmp/vidos_components-$VIDOS_COMP_VER-$$
	DIR=/tmp/vidos_components-$VIDOS_COMP_VER-$$
fi

if [ -z $STYLE ]; then STYLE="ram"; fi
if [ -z $FIRMWARE ]; then FIRMWARE=$DEF_FIRMWARE; fi
if [ -z $BOOTLOADER ]; then BOOTLOADER="bios"; fi
if [ -z $PLAYBACK_COUNT ]; then PLAYBACK_COUNT="1"; fi
MPV_OPTS+=" --loop-playlist="$PLAYBACK_COUNT

checkPaths $DIR "directory"

if [ $ARG_VALID -eq 1 ]; then print_help 2; fi

checkOpts $STYLE "build style" "${STYLE_ARRAY[*]}"
checkOpts $FIRMWARE "graphics drivers" "${FIRMWARE_ARRAY[*]}"
checkOpts $BOOTLOADER "bootloader" "${BOOTLOADER_ARRAY[*]}"
checkOpts $PLAYBACK_COUNT "playback count" "-1 - inf"
checkPaths $DIR "directory"

checkPaths "$VIDEO" "video or video dir"

if [ $ARG_VALID -eq 1 ]; then print_help 2; fi

disk_VID_PATH=$DIR/$VIDOS_DISKFS/video/
ram_VID_PATH=$DIR/initramfs_overlay/opt/
disk_SED_ARG='s/^/\/media\/video\//'
ram_SED_ARG='s/^/\/opt\//'

find $disk_VID_PATH -iregex $SUFFIX_REGEX -delete
find $ram_VID_PATH -iregex $SUFFIX_REGEX -delete

checkVid(){
	. $7/arch/$8/config
	VIDEO_PASS=1
	VID_ENCODING=$(ffprobe -v quiet -show_streams "$1" | grep -w codec_name | sed -n 1p | cut -d'=' -f2)
	VID_NAME=$(echo $FIRST_VID_PATH | rev | cut -d'/' -f1 | cut -d'.' -f2 | rev )
	if [ $VID_ENCODING ]; then
		for item in "${SUPPORTED_VID_CODECS[@]}"
		do
		        if [ $item = $VID_ENCODING ]; then
				VIDEO_PASS=0
				FOUND_FORMAT=${FORMAT_ARRAY[$VID_ENCODING]}
		        fi
        	done
	fi
	if [ $VIDEO_PASS -eq 0 ] && [ $FOUND_FORMAT == $2 ]; then
		cp "$1" $3 && echo $(echo $1 | rev | cut -d'/' -f1 | rev | sed $4 ) >> $6/playlist.txt
		echo "copied" $1
	fi
}

export -f checkVid

for BOOTLOADER_OPTION in "${BOOTLOADER_ARRAY[@]}"; do
        if [ $BOOTLOADER = $BOOTLOADER_OPTION ]; then
		BOOTLOADER_VALID=0
		XORRISO_CMD=$BOOTLOADER_OPTION"_"$AUTHOR_CMD
	fi
done

checkArg $BOOTLOADER_VALID "bootloader" $BOOTLOADER "option" "${BOOTLOADER_ARRAY[*]}"

for STYLE_OPTION in "${STYLE_ARRAY[@]}"; do
        if [ $STYLE = $STYLE_OPTION ]; then
		STYLE_VALID=0
		VID_PATH=$STYLE"_VID_PATH"
		SED_ARG=$STYLE"_SED_ARG"
		if [ $STYLE = "hybrid" ]; then VID_PATH="disk_VID_PATH"; SED_ARG="disk_SED_ARG"; fi
		cp -r $DIR/arch/$ARCH/$STYLE/* $DIR/initramfs_overlay/
	fi
done

checkArg $STYLE_VALID "style" $STYLE "option" "${STYLE_ARRAY[*]}"

handle_firmware

if [ $PLAYBACK_COUNT = "inf" ] || [ $PLAYBACK_COUNT -eq $PLAYBACK_COUNT ]; then
	PLAYBACK_COUNT_VALID=0
fi
echo "mpv options:" $MPV_OPTS
checkArg $PLAYBACK_COUNT_VALID "playback count" $PLAYBACK_COUNT "value" "-1 - inf"
sed -i "s/playlist.txt/playlist.txt $MPV_OPTS/"  $DIR/initramfs_overlay/etc/init.d/S03Video

if [ $VIDEO_DIR ]; then
	FIRST_VID=$(find $VIDEO_DIR -type f | grep -m1 $SUFFIX_REGEX)
	pickCodec "$FIRST_VID"
	find $VIDEO_DIR -type f -iregex $SUFFIX_REGEX -exec bash -c 'checkVid "$0" "$1" "$2" "$3" "$4" "$5" "$6" "$7"' '{}' $VID_FORMAT ${!VID_PATH} ${!SED_ARG} $STYLE ${!VID_PATH} $DIR $ARCH \;
else
	FIRST_VID="${VIDEO_SELECTION[0]}"
	pickCodec "$FIRST_VID"
	echo "format:" $VID_FORMAT
	for SELECTED_VID in "${VIDEO_SELECTION[@]}"; do
		checkVid "$SELECTED_VID" $VID_FORMAT ${!VID_PATH} ${!SED_ARG} $STYLE ${!VID_PATH} $DIR $ARCH
	done
fi

FIRSTVID_NAME=$(echo $FIRST_VID | rev | cut -d'/' -f1 | cut -d'.' -f2 | rev )

if [ $STYLE = "hybrid" ]; then
	mv $disk_VID_PATH/playlist.txt $ram_VID_PATH/playlist.txt
	read -r FIRST_VID_PATH<$ram_VID_PATH/playlist.txt
	FIRSTVID_NAME=$(echo $FIRST_VID_PATH | rev | cut -d'/' -f1 | cut -d'.' -f2 | rev )
	FIRSTVID_FULLNAME=$(echo $FIRST_VID_PATH | rev | cut -d'/' -f1 | rev )
	echo "splitting" $FIRSTVID_FULLNAME "into segments"
	ffmpeg -v quiet -i $disk_VID_PATH/"$FIRSTVID_FULLNAME" -codec copy -map 0 -f segment -segment_list_type m3u8 \
	-segment_list $ram_VID_PATH/playlist.m3u8 -segment_list_entry_prefix /media/video/ \
	-segment_list_flags +cache -segment_time 10 \
	$disk_VID_PATH/"$FIRSTVID_NAME""_segment"%03d.mkv
	echo "updating playlist"
	sed -i '7d' $ram_VID_PATH/playlist.m3u8
	sed -i "6 a /opt/$FIRSTVID_NAME""_segment000.mkv" $ram_VID_PATH/playlist.m3u8
	echo "moving first segment into initramfs"
	mv  $disk_VID_PATH/"$FIRSTVID_NAME"_segment000.mkv $ram_VID_PATH
        sed -i '1s/.*/\/opt\/playlist.m3u8/' $ram_VID_PATH/playlist.txt
	rm $disk_VID_PATH/"$FIRSTVID_FULLNAME"
fi

install_components

copy_bootloader

if [ -z $IMG_NAME ]; then
	IMG_NAME="vidos"_"'""$FIRSTVID_NAME""'"_"$VID_FORMAT"_"$FIRM_DIR"_"$STYLE"_"$BOOTLOADER"_"$(date +%F)"
fi

write_image

cleanUp() {
	find $DIR -iregex $SUFFIX_REGEX -delete
	find $DIR -name "playlist.*" -delete
	find $DIR/initramfs_overlay -name "S03*" -delete
	rm -rf $DIR/initramfs_overlay/usr
	rm -rf $DIR/initramfs_overlay/lib/firmware/*
	rm -rf $DIR/$VIDOS_DISKFS/kernel/*
	rm -rf $DIR/$VIDOS_DISKFS/efi/*
	rm -rf $DIR/ESP/EFI/BOOT/$KERNEL_NAME
	rm -rf $DIR/ESP/EFI/BOOT/rootfs.cpio.lz4
}

cleanUp
