# Hello there!
Thanks for taking some time out of your day to read this, I'm glad you found this project useful/entertaining enough to want to contribute.

As you have probably no doubt noticed, this entire project is a bunch of shell scripts in a trench coat. Ive collected most of the tools I use for actually building and deploying VidOS in the scripts directory, although I should probably do a better job of commenting them.
I'm gonna be honest, I don't think anyone is reading this. and I'm not sure anyone is actually using my project either. But I would love to be proven wrong in any case. So tell you what: If you are reading this, send an email to "sen at hastings dot org" with "update contributing" in the subject line. We can figure it out from there. Cheers!