#!/bin/bash
. scripts/export_vars.sh
SUPPORTED_FORMATS=("av1" "avc" "webm" "efi")
SUPPORTED_STYLES=("disk" "ram" "hybrid")
NAME=$VIDOS_VER

HELP_TEXT=(
"-p platform/archetecture(s) to be included"
"\n""-r release version string"
"\n""-c vidos components version string"
"\n""-v vobu version string")

print_help(){
        echo -e ${HELP_TEXT[@]}
        exit $1
}

while getopts "p:r:c:v:h" opt; do
        case "$opt" in
                p)
                        PLATFORM="$OPTARG";
                        PLATFORM_ARRAY+=($PLATFORM)
                        echo $PLATFORM
                ;;
                r)
			NAME="$OPTARG";
			echo $NAME
                ;;
                c)
			VIDOS_VER="$OPTARG";
			echo $VIDOS_VER
	        ;;
                v)
			VOBU_VER="$OPTARG";
			echo $VOBU_VER
                ;;
		h)
			print_help
                ;;
		*)
			echo "invalid option"
			print_help
                ;;
        esac
done

mkdir -p vidos_release_$NAME/vidos_components-$VIDOS_VER
mkdir -p vidos_release_$NAME-source-and-licence-info/

pushd $BUILDROOT_LATEST

for PLATFORM in "${PLATFORM_ARRAY[@]}"; do
echo "collecting info for" $PLATFORM
mkdir -p ../vidos_release_$NAME-source-and-licence-info/$PLATFORM/
	FORMAT_ARRAY=($(find $PLATFORM -type d -name vidos_release -exec bash -c "echo {} | rev |  cut -d "/" -f3 | rev " \; ))
	for FORMAT in "${FORMAT_ARRAY[@]}"; do
		echo "collecting info for" $PLATFORM":"$FORMAT
	        make O=$PLATFORM/$FORMAT -j$(nproc) legal-info
		cp -r $PLATFORM/$FORMAT/legal-info/* ../vidos_release_$NAME-source-and-licence-info/$PLATFORM/
		cp -r $PLATFORM/$FORMAT/legal-info/manifest.csv ../vidos_release_$NAME-source-and-licence-info/$PLATFORM/$FORMAT-manifest.csv
		cp -r $PLATFORM/$FORMAT/legal-info/host-manifest.csv ../vidos_release_$NAME-source-and-licence-info/$PLATFORM/$FORMAT-host-manifest.csv
		cp -r $PLATFORM/$FORMAT/legal-info/buildroot.config ../vidos_release_$NAME-source-and-licence-info/$PLATFORM/$FORMAT-buildroot.config

		echo $PLATFORM/$FORMAT/images/vidos_release/$FORMAT"_build"/$FORMAT"_kernel"
		cp -r $PLATFORM/$FORMAT/images/vidos_release/$FORMAT"_build"/* ../vidos_release_$NAME/vidos_components-$VIDOS_VER
	done
done
	mv ../vidos_release_$NAME-source-and-licence-info/host-sources ../vidos_release_$NAME-source-and-licence-info/combined-host-sources
	mv ../vidos_release_$NAME-source-and-licence-info/host-licenses ../vidos_release_$NAME-source-and-licence-info/combined-host-licenses
	mv ../vidos_release_$NAME-source-and-licence-info/sources ../vidos_release_$NAME-source-and-licence-info/combined-sources
	mv ../vidos_release_$NAME-source-and-licence-info/licenses ../vidos_release_$NAME-source-and-licence-info/combined-licenses
	rm ../vidos_release_$NAME-source-and-licence-info/host-manifest.csv
	rm ../vidos_release_$NAME-source-and-licence-info/manifest.csv
	rm ../vidos_release_$NAME-source-and-licence-info/buildroot.config
	rm ../vidos_release_$NAME-source-and-licence-info/legal-info.sha256

popd

scripts/prepare_release_readme.sh
cp -r vidos/vobu.sh vidos/test_vids LICENSE.md release_paperwork/README.md release_paperwork/*install.sh vidos_release_$NAME/
cp release_paperwork/LICENCE_README LICENCE.md vidos_release_$NAME-source-and-licence-info/

wget https://cdn.kernel.org/pub/linux/kernel/v$KERNEL_LATEST_MAJOR.x/linux-$KERNEL_LATEST.tar.xz -O vidos_release_$NAME-source-and-licence-info/linux-$KERNEL_LATEST.tar.xz
wget https://buildroot.org/downloads/$BUILDROOT_LATEST.tar.xz -O  vidos_release_$NAME-source-and-licence-info/$BUILDROOT_LATEST.tar.xz
wget https://github.com/systemd/systemd-stable/archive/refs/tags/$SYSTEMD_LATEST.tar.gz -O  vidos_release_$NAME-source-and-licence-info/systemd-stable-$SYSTEMD_LATEST.tar.gz
find vidos_release_$NAME -type f -name "rootfs.cpio.lz4" -delete

sed -i "/^GIT_COMMIT_HASH*/d" vidos_release_$NAME/vobu.sh
sed -i "/^VOBU_VER=*/c\VOBU_VER=${VOBU_VER}" vidos_release_$NAME/vobu.sh
sed -i "/^VIDOS_COMP_VER=*/c\VIDOS_COMP_VER=${VIDOS_VER}" vidos_release_$NAME/vobu.sh

tar -I pigz -cf vidos_release_$NAME.tar.gz vidos_release_$NAME
tar -I pigz -cf legal.tar.gz vidos_release_$NAME-source-and-licence-info
