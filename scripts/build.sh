#!/bin/bash
. scripts/export_vars.sh
BUILDROOT=$BUILDROOT_LATEST
SUPPORTED_SYSTEM_TYPES=("av1" "webm" "avc" "efi")
IS_SUPPORTED=0
TOOLCHAIN=0
PREFIX=0
FORMAT_SET=1
#declare -A FORMAT_ARRAY=()
#FORMAT_OPTION=$1

HELP_TEXT=(
"-p specify system platform/archetecture"
"\n""-f specify video format supported.")

print_help(){
        echo -e ${HELP_TEXT[@]}
        exit $1
}

while getopts ":p:f:h" opt; do
        case "$opt" in
                p)
			PLATFORM="$OPTARG";
			echo $PLATFORM
                ;;
                f)
			FORMAT_SET=0
			FORMAT="$OPTARG";
			FORMAT_ARRAY+=($FORMAT)
			echo $FORMAT
                ;;
		h)
			print_help
                ;;
		*)
			echo "invalid option"
			print_help
                ;;
        esac
done


for FORMAT_OPTION in "${FORMAT_ARRAY[@]}"; do
echo "doin' stuff for" $FORMAT_OPTION
	if [ $FORMAT_SET -eq 1 ]; then
		echo "no configuration specifed, please specify one of the following system types: "${SUPPORTED_SYSTEM_TYPES[@]}&&
		exit 1
	fi

	BUILDROOT_SYSTEM_PATH=$BUILDROOT/$PLATFORM/$FORMAT_OPTION/
	BUILDROOT_IMAGE_PATH=$BUILDROOT_SYSTEM_PATH/images/vidos_release

#	for SYSTEM_TYPE in "${SUPPORTED_SYSTEM_TYPES[@]}"
#	do
#		for FORMAT_OPTION in "${FORMAT_ARRAY[@]}"; do
#			if [ $FORMAT_OPTION = $SYSTEM_TYPE ]; then
#				IS_SUPPORTED=1
#			fi
#		done
#	done

#	if [ $IS_SUPPORTED = 0 ];then
#		echo $FORMAT_OPTION" is not a supported configuration, please specify one of the follwing system types: "${SUPPORTED_SYSTEM_TYPES[@]}&&
#		exit 1
#	fi

#	test -e $BUILDROOT_IMAGE_PATH; BUILDROOT_IMAGE_PATH_EXISTS=$?
#	if [ $BUILDROOT_IMAGE_PATH_EXISTS -eq 0 ]; then
#		VIDOS_ROOTFS=$(ls $BUILDROOT_IMAGE_PATH/ | grep -m 1 "vidos_iso9660_*")
#		if [ "$VIDOS_ROOTFS" != "vidos_iso9660_"$FORMAT_OPTION ]; then
#			yes | rm -r $BUILDROOT
#		else
#			echo "specified vidos distribution VidOS_"$FORMAT_OPTION" has already been built. please run probe.sh"
#			exit 0
#		fi
#	fi
	echo "building VidOS distribution with "$FORMAT_OPTION" support"

	if [ -d $BUILDROOT ]; then
		echo $BUILDROOT "already upacked"
	else
		#download and unpack latest buildroot release
		echo "Downloading buildroot version: "$BUILDROOT &&
		wget -O - https://buildroot.org/downloads/$BUILDROOT.tar.gz | tar zxf - &&
		echo "finished unpacking "$BUILDROOT &&
		#move configuration dir into board dir
		cp -r vidos $BUILDROOT/board/ &&
		echo "moved vidos dir to "$BUILDROOT
	fi

	echo $BUILDROOT_SYSTEM_PATH
	if [ -e $BUILDROOT_SYSTEM_PATH ]; then
		echo $BUILDROOT_SYSTEM_PATH "already created"
	else
		echo "making system path" $BUILDROOT_SYSTEM_PATH
		mkdir -p $BUILDROOT_SYSTEM_PATH
		echo "installing" $PLATFORM "specific patches"
		if [ -e	scripts/$PLATFORM/install-patches.sh ]; then
			scripts/$PLATFORM/install-patches.sh
		fi
	fi

	if [ -e $BUILDROOT_IMAGE_PATH ]; then
		echo $BUILDROOT_IMAGE_PATH "already built"
	else
		echo "patching base configuration for "$FORMAT_OPTION" support" &&
	 	patch configs/$PLATFORM/vidos_base_config patches/$PLATFORM/vidos_$FORMAT_OPTION.patch -o "vidos_"$FORMAT_OPTION"_config" &&
		cp --verbose "vidos_"$FORMAT_OPTION"_config" $BUILDROOT/$PLATFORM/$FORMAT_OPTION/.config &&
		pushd $BUILDROOT/ &&
		make O=$PLATFORM/$FORMAT_OPTION olddefconfig &&
		make O=$PLATFORM/$FORMAT_OPTION -j$(nproc)
		popd
	fi

	echo "Build for VidOS_"$FORMAT_OPTION" successful!"
done
