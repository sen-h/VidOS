#!/bin/sh
BUILDROOT_LATEST=buildroot-2023.11
FFMPEG_VERSION=$(grep "FFMPEG_VERSION =" $BUILDROOT_LATEST/package/ffmpeg/ffmpeg.mk | cut -d " " -f3)
RPI_FIRMWARE_VERSION=$(grep "RPI_FIRMWARE_VERSION =" $BUILDROOT_LATEST/package/rpi-firmware/rpi-firmware.mk | cut -d " " -f3)


download_ffmpeg(){
if [ ! -e ffmpeg_4.3.6-0+deb11u1+rpt5.debian.tar.xz ]; then
	echo "downloading ffmpeg 4.3.6 package from raspberrypi.org package repo"
	wget http://archive.raspberrypi.org/debian/pool/main/f/ffmpeg/ffmpeg_4.3.6-0+deb11u1+rpt5.debian.tar.xz
fi
}

if [ $FFMPEG_VERSION != 4.3.6 ]; then
	echo "backing up ffmpeg 4.4.4 patches"
	mv $BUILDROOT_LATEST/package/ffmpeg/*.patch patches/x86_64/ffmpeg
	download_ffmpeg
	echo "extracting mystery-meat(TM) patches to enable hw acceleration"
	tar -xf ffmpeg_4.3.6-0+deb11u1+rpt5.debian.tar.xz debian/patches/ffmpeg-4.3.6-rpi_21.patch
	echo "installing patches"
	mv debian/patches/ffmpeg-4.3.6-rpi_21.patch $BUILDROOT_LATEST/package/ffmpeg/0001-ffmpeg-4.3.6-rpi_21.patch
	echo "destroying evidence"
	rm -r debian

	echo "deploying patches to rollback ffmpeg from version 4.4.4 to 4.3.6"
	cp patches/arm64/raspberrypi4-64/rpi-firmware/rpi-firmware.* $BUILDROOT_LATEST/
	cp patches/arm64/raspberrypi4-64/ffmpeg/ffmpeg.* $BUILDROOT_LATEST/
	cd $BUILDROOT_LATEST/

	echo "applying rollback patches"

	patch -p0 <ffmpeg.mk.patch
	patch -p0 <ffmpeg.hash.patch

	if [ $RPI_FIRMWARE_VERSION != 1.20230405 ]; then
		echo "updating rpi-firmware to 1.20230405"
		patch -p0 <rpi-firmware.mk.patch
		patch -p0 <rpi-firmware.hash.patch
	else
		echo "rpi-firmware already updated to 1.20230405"
	fi
else
	echo "ffmpeg version is already set to 4.3.6"
fi
