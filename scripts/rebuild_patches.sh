#!/bin/bash
. scripts/export_vars.sh
#BUILDROOT_LATEST="buildroot-2023.11"
SUPPORTED_FORMATS=("av1" "avc" "webm" "efi")
SUPPORTED_STYLES=("disk" "ram" "hybrid")

HELP_TEXT=(
"-p specify platform."
"\n""-b specify build style.")

print_help(){
        echo -e ${HELP_TEXT[@]}
        exit $1
}

while getopts ":p:b:h" opt; do
        case "$opt" in
                p)
			PLATFORM="$OPTARG";
			echo $PLATFORM
                ;;
                b)
			FORMAT="$OPTARG";
			echo $FORMAT
                ;;
		h)
			print_help
                ;;
        esac
done

#this script generates new patches for the vidos format configs
#if you have made a change to a particular format, run this script so it gets preserved :>)

pushd $BUILDROOT_LATEST

#first savedefconfigs are created for each format
	make O=$PLATFORM/$FORMAT -j$(nproc) savedefconfig BR2_DEFCONFIG=../configs/$PLATFORM/"vidos_"$FORMAT"_config"
	echo "vidos_"$FORMAT"_config"

popd
pushd configs

#then those savedefconfigs are compared against the base config to create patches
	diff -U0 $PLATFORM/vidos_base_config  $PLATFORM/"vidos_"$FORMAT"_config" | cut -f1 > ../patches/$PLATFORM/vidos_$FORMAT.patch
	echo "vidos_$FORMAT.patch"
