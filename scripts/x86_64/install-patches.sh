#!/bin/sh
BUILDROOT_LATEST=buildroot-2023.11
FFMPEG_VERSION=$(grep "FFMPEG_VERSION =" $BUILDROOT_LATEST/package/ffmpeg/ffmpeg.mk | cut -d " " -f3)

#cp patches/0018-quiet-isolinux-test.patch $BUILDROOT_LATEST/boot/syslinux/

#echo "installed syslinux patch!"

if [ $FFMPEG_VERSION != 4.4.4 ]; then
	echo "deploying backed up ffmpeg 4.4.4 patches"
	if [ -e $BUILDROOT_LATEST/package/ffmpeg/0001-ffmpeg-4.3.6-rpi_21.patch ]; then
		rm -rf $BUILDROOT_LATEST/package/ffmpeg/0001-ffmpeg-4.3.6-rpi_21.patch
	fi
	cp patches/x86_64/ffmpeg/0*.patch $BUILDROOT_LATEST/package/ffmpeg/
	echo "deploying patches to update ffmpeg version 4.3.6 to 4.4.4"
	cp patches/x86_64/ffmpeg/ffmpeg.hash.patch $BUILDROOT_LATEST/
	cp patches/x86_64/ffmpeg/ffmpeg.mk.patch $BUILDROOT_LATEST/
	cd $BUILDROOT_LATEST/

	echo "applying update patches"
	patch -p0 <ffmpeg.mk.patch
	patch -p0 <ffmpeg.hash.patch

else
	echo "ffmpeg version is already set to 4.4.4"
fi
